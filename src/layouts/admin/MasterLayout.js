import React from 'react';
import { Routes, Route } from 'react-router-dom';
import '../../assets/admin/css/styles.css';
import '../../assets/admin/js/scripts';

import Navbar from './Navbar';
import Sidebar from './Sidebar';
import Footer from './Footer';

import routes from '../../routes/routes';
import NotFound from '../../components/admin/NotFound';

const MasterLayout = () => {

    return (
        <div className="sb-nav-fixed">
            <Navbar />
            <div id="layoutSidenav">
                <div id="layoutSidenav_nav">
                    <Sidebar />
                </div>

                <div id="layoutSidenav_content">
                    {/* Page content */}
                    <main>
                        <Routes>
                            <Route path="*" element={<NotFound />} />

                            {routes.map((route, idx) => {
                                return (
                                    route.element && (
                                        <Route
                                            key={idx}
                                            path={route.path}
                                            name={route.name}
                                            exact={route.exact}
                                            element={<route.element to={route.to}/>}
                                        />
                                    )
                                );
                            })}

                        </Routes>
                    </main>

                    <Footer />
                </div>
            </div>
        </div>
    );
}

export default MasterLayout;