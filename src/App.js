import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import MasterLayout from './layouts/admin/MasterLayout';

function App() {
  return (
    <div className="App">
      <Router>
        <MasterLayout />
        {/* <Routes> */}
          {/* <Route path="/admin" element={<MasterLayout {...props} />} /> */}
        {/* </Routes> */}
      </Router>
    </div>
  );
}

export default App;
